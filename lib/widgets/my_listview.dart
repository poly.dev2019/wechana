import 'package:flutter/material.dart';

class MyListView extends StatelessWidget {
  final String title;
  final Icon icon;

  MyListView({@required this.title, this.icon});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Card(
            child: ListTile(
          title: Text(title,
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
          subtitle: Text("ใช้บริการอย่างมั่นใจ มีใบรับรอง"),
          trailing: Icon(Icons.keyboard_arrow_right),
          leading: CircleAvatar(
            backgroundImage: AssetImage(
                "assets/images/logo.png"), // no matter how big it is, it won't overflow
          ),
        ))
      ],
    );
  }
}
