import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:wechana/widgets/app_bar.dart';

class CustomCard extends StatelessWidget {
  final String screenTitle;
  final IconData icon;
  final String title;
  final String subTitle;
  final Function cancelBtn;
  final Function proccessBtn;

  CustomCard(
      {@required this.screenTitle,
      @required this.icon,
      @required this.title,
      @required this.subTitle,
      @required this.cancelBtn,
      @required this.proccessBtn});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Hexcolor('#F5F5F5'),
      appBar: new MyAppBar(
        title: "เราต้องชนะ",
        // icon: Icons.access_alarm,
        imageLogo: Image.asset("assets/images/logo.png",
            fit: BoxFit.contain, height: 70),
      ),
      // body: ,
    );
  }
}
