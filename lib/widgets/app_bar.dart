import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final IconData icon;
  final Image imageLogo;

  MyAppBar({@required this.title, this.icon, @required this.imageLogo});
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            child: imageLogo,
            padding: const EdgeInsets.only(top: 45.0),
          ),
          Container(
              padding: const EdgeInsets.only(top: 30.0),
              child: Text(title, style: TextStyle(color: Colors.black)))
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(100.0);
}
