import 'package:flutter/material.dart';
import 'package:wechana/widgets/app_bar.dart';
import 'package:wechana/widgets/my_listview.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: "เราต้องชนะ",
        imageLogo: Image.asset("assets/images/logo.png",
            fit: BoxFit.contain, height: 70),
      ),
      body: MyListView(
        title: "ค้นหาร้านค้า"
      ),
    );
  }
}
